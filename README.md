# recipe-app-api-proxy-mine

#Recipe app API proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (Default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the ap to forward requests to (default: `9000`)

